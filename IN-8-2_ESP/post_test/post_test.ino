#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

#ifndef STASSID
#define STASSID ""
#define STAPSK  ""
#endif

const char* ssid     = STASSID;
const char* password = STAPSK;

ESP8266WebServer server(80);

const int led = LED_BUILTIN;

String postForms = "<html>\
  <head>\
    <title>ESP8266 Web Server POST handling</title>\
    <style>\
      body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
    </style>\
  </head>\
  <body>\
    <h1>Hour adjusting</h1><br>\
  <form method=\"post\" enctype=\"application/x-www-form-urlencoded\" action=\"/hour\">\
      <input type=\"text\" name=\"hour\"><br>\
      <input type=\"submit\" value=\"Submit\">\
    </form>\
    <h1>Minute adjusting</h1><br>\
    <form method=\"post\" enctype=\"application/x-www-form-urlencoded\" action=\"/minute\">\
      <input type=\"text\" name=\"minute\"><br>\
      <input type=\"submit\" value=\"Submit\">\
    </form>\
    <h1>Second adjusting</h1><br>\
    <form method=\"post\" enctype=\"application/x-www-form-urlencoded\" action=\"/sec\">\
      <input type=\"text\" name=\"second\"><br>\
      <input type=\"submit\" value=\"Submit\">\
    </form>\
  </body>\
</html>";

void handleRoot() {
  server.send(200, "text/html", postForms);
}

void handleHour() {
  if (server.method() != HTTP_POST) {
    server.send(405, "text/plain", "Method Not Allowed");
  } else {
    Serial.println("Hour");
    Serial.println(server.argName(0) + " " + server.arg(0));
    server.send(200, "text/html", postForms);
  }
}

void handleSecond() {
  if (server.method() != HTTP_POST) {
    server.send(405, "text/plain", "second");
  } else {
    Serial.println("Sec");
    Serial.println(server.argName(0) + " " + server.arg(0));
    server.send(200, "text/html", postForms);
  }
}

void handleMinute() {
  if (server.method() != HTTP_POST) {
    server.send(405, "text/plain", "Method Not Allowed");
  } else {
    Serial.println("Minute");
    for (uint8_t i = 0; i < server.args(); i++) {
      Serial.println(server.argName(i) + " " + server.arg(i));
    }
    server.send(200, "text/html", postForms);
  }
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";

  }
  server.send(404, "text/plain", message);
}

void setup(void) {
  pinMode(led, OUTPUT);
  digitalWrite(led, 0);
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);

  server.on("/hour", handleHour);

  server.on("/minute", handleMinute);

  server.on("/sec", handleSecond);

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void) {
  server.handleClient();
}
