#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include "Arduino.h"
#include "Wire.h"
#include "RTClib.h"
#include <TimeLib.h>
#include <Streaming.h>

MDNSResponder mdns;

// впишите сюда данные, соответствующие вашей сети:
const char* ssid = "Nixie_Clock_1";
const char* password = "12345";

#define IP "192.168.0.100"
#define GATEWAY "192.168.0.2"
#define SUBNET "255.255.255.0"

ESP8266WebServer server(80);
RTC_DS3231 rtc;
String webPage = "";

//////Входа///////////////////////////////////////////////////////////////////
byte led = 5;
//макс изм напряжение ADC это 1в, а не 3.3.
byte clockPin = 15;
byte latchPin = 13;
byte dataPin = 12;
//yte valve3 = 13;

/*Qa - 1tr
  Qb - 2tr
  ..
  QH -8tr
  Qa2 - 9tr
  Qc2 - 11tr

  Qd2 - 1 lamp
  Qg2 - 4 lamp
*/
///////Переменные/////////////////////////////////////////////////////////////
unsigned long curTime = 0; //для организации циклов прерывания
unsigned long last_time_scroll = 0;
//unsigned long dotTime = 0;
const int interval = 10000; //основной цикл
//const int secint = 200; //для секундной точки
const int scroll_int = 3600000; //интервал прогона по цифрам час
//const int scroll_int = 10000; //интервал прогона по цифрам час
//const byte Sync_interval = 3;//интервал синхронизации времени

//10 цифр                             0  1  2   3   4   5    6   7    8   9
const int dictionary_of_number[11] = {2, 1, 4, 16, 64, 256, 512, 128, 32, 8};
//4 лампы и 1 точка
const int dictionary_of_lamp[8] = {1024, 2048, 4096, 8192, 16384, 32768 };
int time_date[] = {0, 0, 0, 0}; // массив где хранится временя или дата

bool debug = true;
bool test_on = true;
bool dot = true;
bool hot_alarm = false;

String postForms = "<html>\
  <head>\
    <title>ESP8266 Web Server for time adjusting</title>\
    <style>\
      body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
    </style>\
  </head>\
  <body>\
    <h3>Hour adjusting</h3><br>\
  <form method=\"post\" enctype=\"application/x-www-form-urlencoded\" action=\"/hour\">\
      <input type=\"text\" name=\"hour\"><br>\
      <input type=\"submit\" value=\"Change\">\
    </form>\
    <h3>Minute adjusting</h3><br>\
    <form method=\"post\" enctype=\"application/x-www-form-urlencoded\" action=\"/minute\">\
      <input type=\"text\" name=\"minute\"><br>\
      <input type=\"submit\" value=\"Change\">\
    </form>\
    <h3>Second adjusting</h3><br>\
    <form method=\"post\" enctype=\"application/x-www-form-urlencoded\" action=\"/sec\">\
      <input type=\"text\" name=\"second\"><br>\
      <input type=\"submit\" value=\"Change\">\
    </form>\
  </body>\
</html>";


void handleRoot() {
  server.send(200, "text/html", postForms);
}

void handleHour() {
  if (server.method() != HTTP_POST) {
    server.send(405, "text/plain", "Method Not Allowed");
  } else {
    Serial.println("Hour");
    // for (uint8_t i = 0; i < server.args(); i++) {
    // Serial.println(server.argName(i) + " " + server.arg(i));
    set_timeFrom_server(true, false, false, server.arg(0));
    // break;
    //}
    server.send(200, "text/html", postForms);
  }
}

void handleSecond() {
  if (server.method() != HTTP_POST) {
    server.send(405, "text/plain", "second");
  } else {
    Serial.println("Sec");
    // for (uint8_t i = 0; i < server.args(); i++) {
    // Serial.println(server.argName(i) + " " + server.arg(i));
    set_timeFrom_server(false, false, true, server.arg(0));
    //  break;
    //}
    server.send(200, "text/html", postForms);
  }
}

void handleMinute() {
  if (server.method() != HTTP_POST) {
    server.send(405, "text/plain", "Method Not Allowed");
  } else {
    Serial.println("Minute");
    // for (uint8_t i = 0; i < server.args(); i++) {
    //Serial.println(server.argName(i) + " " + server.arg(i));
    set_timeFrom_server(false, true, false, server.arg(0));
    // break;
    //  }
    server.send(200, "text/html", postForms);
  }
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";

  }
  server.send(404, "text/plain", message);
}

void setup(void) {
  Serial.begin(115200);

  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);

  WiFi.softAP(ssid, password);
  IPAddress ip;
  IPAddress gateway;
  IPAddress subnet;
  ip.fromString(IP);
  gateway.fromString(GATEWAY);
  subnet.fromString(SUBNET);

  WiFi.config(ip, gateway, subnet);

  IPAddress myIP = WiFi.softAPIP();

  Serial.print("AP IP address: ");
  Serial.println(myIP);
  //server.begin();

  Wire.begin();

  if (! rtc.begin()) Serial.println("Couldn't find RTC");

  if (rtc.lostPower()) {
    Serial.println("RTC lost power, lets set the time!");
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }

  //setTime(getTime());
  // setSyncProvider(time_to_date());

  last_time_scroll = millis();
  curTime = millis();
  scroll(25);
  if (debug)Serial.println("Setup");

  if (mdns.begin("esp8266", WiFi.localIP())) {
    Serial.println("MDNS responder started");
    //  "Запущен MDNSresponder"
  }

  server.on("/", handleRoot);
  server.on("/hour", handleHour);
  server.on("/minute", handleMinute);
  server.on("/sec", handleSecond);
  server.onNotFound(handleNotFound);
  server.begin();

  Serial.println("HTTP server started");
}

void digitalClockDisplay()
{
  Serial.print(rtc.now().hour());
  printDigits(rtc.now().minute());
  printDigits(rtc.now().second());
  Serial.print(' ');
  Serial.print(rtc.now().day());
  Serial.print(' ');
  Serial.print(rtc.now().month());
  Serial.print(' ');
  Serial.print(rtc.now().year());
  Serial.println();
}

void printDigits(int digits) {
  Serial.print(':');
  if (digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

//вывод времени
void show(int a[])
{
  for (int i = 0; i < 4; i++)
  {
    digitalWrite(latchPin, LOW);

    int res = 0;
    res = dictionary_of_number[a[i]] | dictionary_of_lamp[i];
    if (dot) res = res | dictionary_of_lamp[4];
    /*
        Serial.println(dictionary_of_number[a[i]], BIN);
        Serial.println(dictionary_of_lamp[i], BIN);
        Serial.println(res, BIN);
        Serial.println("================================================");
    */
    shiftOut(dataPin, clockPin, MSBFIRST, (res >> 8));
    shiftOut(dataPin, clockPin, MSBFIRST, res);
    //"защелкиваем" регистр, тем самым устанавливая значения на выходах
    digitalWrite(latchPin, HIGH);
    delay(3);
    digitalWrite(latchPin, LOW);
    // delay(2);
  }
}

//обращение к модулю и сохрание времени в массив
void check_time_date() {
  time_date[0] = rtc.now().hour() / 10;
  time_date[1] = rtc.now().hour() % 10;
  time_date[2] = rtc.now().minute()  / 10;
  time_date[3] = rtc.now().minute()  % 10;
  if (rtc.now().second() % 2 > 0) dot = true; else dot = false;
}

void set_time() {
  //2018,12,3,19,50,00
  //2019,09,12,21,06,00
  // check for input to set the RTC, minimum length is 12, i.e. yy,m,d,h,m,s
  if (Serial.available() >= 12) {
    static time_t tLast;
    time_t t;
    tmElements_t tm;
    // note that the tmElements_t Year member is an offset from 1970,
    // but the RTC wants the last two digits of the calendar year.
    // use the convenience macros from the Time Library to do the conversions.
    int y = Serial.parseInt();
    if (y >= 100 && y < 1000)
      Serial << F("Error: Year must be two digits or four digits!") << endl;
    else {
      if (y >= 1000)
        tm.Year = CalendarYrToTm(y);
      else    // (y < 100)
        tm.Year = y2kYearToTm(y);
      tm.Month = Serial.parseInt();
      tm.Day = Serial.parseInt();
      tm.Hour = Serial.parseInt();
      tm.Minute = Serial.parseInt();
      tm.Second = Serial.parseInt();
      t = makeTime(tm);
      //  RTCLib::set(byte second, byte minute, byte hour, byte dayOfWeek, byte dayOfMonth, byte month, byte year)
      setTime(t);
      //rtc.set(second(), minute(), hour(), weekday(), day(), month(), year()); // use the time_t value to ensure correct weekday is set
      rtc.adjust(t);
      Serial << F("RTC set to: ");
      digitalClockDisplay();
      Serial << endl;
      // dump any extraneous input
      while (Serial.available() > 0) Serial.read();
    }
  }
}

int argsToInt(String param) {
  int res = 0;
  String str;
  if (param.length() <= 2) {
    res = param.toInt();
  }
  return res;
}

void set_timeFrom_server(bool h, bool m, bool s, String param) {
  int hr = 0;
  int mt = 0;
  int sd = 0;
  if (debug)Serial.print("param: ");
  if (debug)Serial.println(param);

  if (h) {
    hr = argsToInt(param);
    if (debug)Serial.print("h= ");
    if (debug)Serial.println(hr);
    if (hr >= 0 & hr <= 23) {
      if (debug) Serial.println("hour adjust");
      rtc.adjust(DateTime(rtc.now().year(), rtc.now().month(),  rtc.now().day(), hr,  rtc.now().minute(),  rtc.now().second() ));
      setTime(time_to_date());
    }
  } else if (m) {
    mt =  argsToInt(param);

    if (debug) Serial.print("m= ");
    if (debug)Serial.println(mt);
    if (mt >= 0 & mt <= 59) {
      if (debug)Serial.println("minite adjust");
      rtc.adjust(DateTime(rtc.now().year(), rtc.now().month(),  rtc.now().day(), rtc.now().hour(),  mt,  rtc.now().second() ));
      setTime(time_to_date());
    }
  } else if (s) {
    sd =  argsToInt(param);
    if (debug)Serial.print("s= ");
    if (debug)Serial.println(sd);
    if (sd >= 0 & sd <= 59) {
      if (debug) Serial.println("second adjust");
      rtc.adjust(DateTime(rtc.now().year(), rtc.now().month(),  rtc.now().day(), rtc.now().hour(),  rtc.now().minute(), sd ));
      setTime(time_to_date());
    }
  }
}

//пеhtбора всех цифр
void scroll(int pause) {
  for (int i = 0; i <= 9; i++) {
    int a[] = {i, i, i, i};
    show(a);
    delay(pause);
    dot = !dot;
    show(a);
  }
}

void temp_ctrl() {
  float c = rtc.getTemperature();
  float f = c * 9. / 5. + 32.;
  Serial << F("  ") << c << F(" C  ") << f << F(" F");
  if (c >= 60 && !rtc.lostPower()) hot_alarm = true;
  if (hot_alarm & c <= 50) hot_alarm = false;
  if (debug)Serial.println();
}

void do_idle() {
  if (debug)Serial.println("idle");
  for (int i = 0; i < 4; i++) {
    digitalWrite(latchPin, LOW);
    shiftOut(dataPin, clockPin, MSBFIRST, 0);
    shiftOut(dataPin, clockPin, MSBFIRST, 0);
    //"защелкиваем" регистр, тем самым устанавливая значения на выходах
    digitalWrite(latchPin, HIGH);
  }
  delay(60000);
}

time_t time_to_date() {
  DateTime dt = rtc.now();
  time_t t = dt.unixtime();
  return t;
}

void loop(void) {
  server.handleClient();
  //делам прогон всех цифр во избежание "Отравление" катодов ламп
  if (!hot_alarm) {

    if (millis() - last_time_scroll > scroll_int ) {
      scroll(25);
      if (debug) Serial.println("Scroll");
      last_time_scroll = millis();
    } else if (last_time_scroll > millis()) {
      last_time_scroll = millis();
    }

    //раз в 10 сек
    if (millis() >= curTime + interval ) {
      for (int i = 0; i < 4; i++) {
        if (debug)Serial.print(time_date[i]);
      }
      if (debug) Serial.println();
      setTime(time_to_date());
      digitalClockDisplay();
      temp_ctrl();
      curTime = millis();
    } else if (curTime > millis()) {
      curTime = millis();
    }

    set_time();
    check_time_date();
    show(time_date);
  } else {
    do_idle();
    temp_ctrl();
  }
}
