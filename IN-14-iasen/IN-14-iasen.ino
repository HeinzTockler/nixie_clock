#include <DS3232RTC.h>
#include <Wire.h>
#include <Streaming.h>
#include <TimeLib.h>
#include <ACI_10K_an.h>

//////Входа//////////////////////////////////////////////////////////////////////
byte but_l = 7; //пин для считывания кнопок
byte but_r = 8; //пин для считывания кнопок
byte but_s = 9; //пин для считывания кнопок

byte dot_pin = 2; //пин для точки
const int def_pin[] = {12, 10, 11, 13};//выводы для дешифратора defPin[out1,out2,out4,out8];
byte led = 14; //пин для светодиодов
const int keys_pin[] = {6, 5, 4, 3}; // выводы для транзисторных ключей
byte ntc = A1; //пин для термометра
byte fan = 21; //пин для вентилятора
const byte menudelay = 3;

///////Переменные/////////////////////////////////////////////////////////////
unsigned long curTime = 0; //для организации циклов прерывания
unsigned long last_time_scroll = 0;
unsigned long butPressT = 0;
//unsigned long dotTime = 0;
const int interval = 10000; //основной цикл
//const int secint = 200; //для секундной точки
//const int scroll_int = ‭90000‬; //интервал прогона по цифрам полчаса
const int scroll_int = 900000; //интервал прогона по цифрам час
//const byte Sync_interval = 3;//интервал синхронизации времени
const char dictionary_of_number[10][4] = { //словарь цифр
  {1, 0, 0, 0},//0
  {1, 0, 0, 1},//1
  {0, 0, 1, 0},//2
  {0, 1, 1, 1},//3
  {0, 1, 0, 0},//4
  {0, 0, 0, 1},//5
  {0, 0, 0, 0},//6
  {0, 1, 0, 1},//7
  {0, 1, 1, 0},//8
  {0, 0, 1, 1},//9
};

int h = 0; //переменная для настройки часов
int m = 0; //переменная для настройки минут
int time_date[] = {0, 0, 0, 0}; // массив где хранится временя или дата

bool debug = true;
bool h_m_swap = false; //флаг выбора минут или часов true=минуты
bool dot = true; //флаг для отображения точки
bool hot_alarm = false; //флаг перегрева
bool butLong = false; // флаг длительного нажатия
bool menu = false; // флаг входа в меню
bool min_select = false;
bool hour_select = false;

Aci_10K an10k;

void setup() {
  Serial.begin(115200);

  setSyncProvider(RTC.get);   // the function to get the time from the RTC
  setSyncInterval(3600);
  Serial << F("RTC Sync");
  if (timeStatus() != timeSet) Serial << F(" FAIL!");
  Serial << endl;

  for (int i = 0; i < 4; i++) {
    pinMode(def_pin[i], OUTPUT);
  }
  for (int i = 0; i < 6; i++) {
    pinMode(keys_pin[i], OUTPUT);
  }
  pinMode(led, OUTPUT);
  pinMode(dot_pin, OUTPUT);
  pinMode(ntc, INPUT);
  pinMode(fan, OUTPUT);

  last_time_scroll = millis();
  curTime = millis();
  scroll(200);
  if (debug)Serial.println("Setup");
  digitalClockDisplay();
  // testnum();
}

void digitalClockDisplay()
{
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.print(' ');
  Serial.print(day());
  Serial.print(' ');
  Serial.print(month());
  Serial.print(' ');
  Serial.print(year());
  Serial.println();
}

void printDigits(int digits) {
  Serial.print(':');
  if (digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

//вывод времени
void show(int a[])
{
  if (!menu) {
    for (int i = 0; i < 4; i++)
    {
      set_number(a[i]);//передаем сигналы для a[i] цифры
      digitalWrite(keys_pin[i], HIGH);//подаем сигнал на keysPin[i] индикатор
      delay(2);
      digitalWrite(keys_pin[i], LOW);//потушим keysPin[i] индикатор
      delay(2);
    }
  } else if (dot) {
    if (min_select) {
      for (int i = 0; i < 4; i++)
      {
        if (i < 2) set_number(9); else set_number(a[i]); //передаем сигналы для a[i] цифры
        digitalWrite(keys_pin[i], HIGH);//подаем сигнал на keysPin[i] индикатор
        delay(2);
        digitalWrite(keys_pin[i], LOW);//потушим keysPin[i] индикатор
        delay(2);
      }
    }

    if (hour_select) {
      for (int i = 0; i < 4; i++)
      {
        if (i > 1) set_number(9); else set_number(a[i]); //передаем сигналы для a[i] цифры
        digitalWrite(keys_pin[i], HIGH);//подаем сигнал на keysPin[i] индикатор
        delay(2);
        digitalWrite(keys_pin[i], LOW);//потушим keysPin[i] индикатор
        delay(2);
      }
    }

    if (!hour_select & !min_select) {
      for (int i = 0; i < 4; i++)
      {
        set_number(a[i]);//передаем сигналы для a[i] цифры
        digitalWrite(keys_pin[i], HIGH);//подаем сигнал на keysPin[i] индикатор
        delay(2);
        digitalWrite(keys_pin[i], LOW);//потушим keysPin[i] индикатор
        delay(2);
      }
    }

  } else {
    if (h_m_swap) { //часы но отображаем мы минуты
      for (int i = 0; i < 2; i++)
      {
        if (min_select) set_number(9); else set_number(a[i]); //передаем сигналы для a[i] цифры
        digitalWrite(keys_pin[i], HIGH);//подаем сигнал на keysPin[i] индикатор
        delay(2);
        digitalWrite(keys_pin[i], LOW);//потушим keysPin[i] индикатор
        delay(2);
      }
    } else {
      //минуты но отображаем мы часы
      for (int i = 2; i < 4; i++)
      {
        if (hour_select) set_number(9); else set_number(a[i]);//передаем сигналы для a[i] цифры
        digitalWrite(keys_pin[i], HIGH);//подаем сигнал на keysPin[i] индикатор
        delay(2);
        digitalWrite(keys_pin[i], LOW);//потушим keysPin[i] индикатор
        delay(2);
      }
    }
  }
}

//вывод опеределенной цифры
void set_number(int num) {
  for (int i = 0; i < 4; i++) {//цикл по словарю и взависимости от цифры в словаре подаем сигнал на к155ид1
    digitalWrite(def_pin[i], dictionary_of_number[num][i]);
  }
}

//пеhtбора всех цифр
void scroll(int pause) {
  for (int j = 0; j <= 9; j++) {
    int a[] = {j, j, j, j};
    for (int i = 0; i < 4; i++)
    {
      set_number(a[i]);//передаем сигналы для a[i] цифры
      digitalWrite(keys_pin[i], HIGH);//подаем сигнал на keysPin[i] индикатор
    }
    delay(pause);
    for (int i = 0; i < 4; i++)
    {
      digitalWrite(keys_pin[i], LOW);//потушим keysPin[i] индикатор
    }
  }
}

//обращение к модулю и сохрание времени в массив
void check_time_date() {
  if (!menu) {
    time_date[0] = hour() / 10;
    time_date[1] = hour() % 10;
    time_date[2] = minute()  / 10;
    time_date[3] = minute()  % 10;
  } else {
    time_date[0] = h / 10;
    time_date[1] = h % 10;
    time_date[2] = m / 10;
    time_date[3] = m % 10;
  }
  if (second() % 2 > 0) dot = true; else dot = false;
}

void testnum() {
  //0842619
  //0123456789
  if (debug)Serial.println("test");

  for (int j = 0; j < 3; j = j + 2)
    for (int i = 0; i < 10; i++) {
      if (debug)Serial.println(i);
      delay(1000);
      set_number(i);
      digitalWrite(keys_pin[j + 1], HIGH);
      digitalWrite(keys_pin[j], HIGH);
      delay(3000);
      digitalWrite(keys_pin[j + 1], LOW);
      digitalWrite(keys_pin[j], LOW);
    }

}

void dot_chek() {
  if (dot) digitalWrite(dot_pin, HIGH); else  digitalWrite(dot_pin, LOW);
}

void led_ctrl() {

}

void set_time() {
  //2019,06,12,12,47,00
  // check for input to set the RTC, minimum length is 12, i.e. yy,m,d,h,m,s
  if (Serial.available() >= 12) {
    static time_t tLast;
    time_t t;
    tmElements_t tm;
    // note that the tmElements_t Year member is an offset from 1970,
    // but the RTC wants the last two digits of the calendar year.
    // use the convenience macros from the Time Library to do the conversions.
    int y = Serial.parseInt();
    if (y >= 100 && y < 1000)
      Serial << F("Error: Year must be two digits or four digits!") << endl;
    else {
      if (y >= 1000)
        tm.Year = CalendarYrToTm(y);
      else    // (y < 100)
        tm.Year = y2kYearToTm(y);
      tm.Month = Serial.parseInt();
      tm.Day = Serial.parseInt();
      tm.Hour = Serial.parseInt();
      tm.Minute = Serial.parseInt();
      tm.Second = Serial.parseInt();
      t = makeTime(tm);
      RTC.set(t);        // use the time_t value to ensure correct weekday is set
      setTime(t);
      Serial << F("RTC set to: ");
      digitalClockDisplay();
      Serial << endl;
      // dump any extraneous input
      while (Serial.available() > 0) Serial.read();
    }
  }
}

void temp_ctrl() {
  float c = RTC.temperature() / 4.;
  float f = c * 9. / 5. + 32.;
  Serial << F("  ") << c << F(" C  ") << f << F(" F");
  //метод получения температуры с датчика NTC и расчет значения
  /*
    const float A_const = 3.354016e-3;
    const float B_const = 2.569107e-4;
    const float C_const = 2.626311e-6;
    const float D_const = 0.675278e-7;
    float tmp = 0; //ouput variable

    float logRom = 0, TKelvin = 0;
    float ACP = 0;
    float Rom = 0;

    ACP = analogRead(ntc);
    // if (debug) Serial.println((String)"ACP= " + ACP);

    Rom = 10000 * (1024 - ACP) /ACP ;
    // if (debug) Serial.println((String)"Rom= " + Rom);

    logRom = log(Rom / 10000);
    TKelvin = 1.0 / (A_const + B_const * logRom + C_const * logRom * logRom + D_const * logRom * logRom * logRom);
    // if (debug)Serial.println((String)"TKelvin= " + TKelvin);

    tmp = TKelvin - 273.15;

    //if (debug)Serial.print("temp= ");
    //if (debug)Serial.println(tmp);
  */
  if (c >= 60) hot_alarm = true;
  if (hot_alarm & c <= 50) hot_alarm = false;
  if (debug)Serial.println();
}

/*
   а этот отправляет индикацию в аут на 1 минуту, а потом снова смотрит что и как
*/
void do_idle() {
  if (debug)Serial.println("idle");
  for (int i = 0; i < 4; i++) {
    digitalWrite(keys_pin[i], LOW);
  }
  digitalWrite(dot_pin, LOW);
  delay(60000);
}

void menu_init() {
  butLong = false;
  butPressT = 0;
  h = hour();
  m = minute();
  menu = true;
  for (int i = 0; i < 4; i++) {
    digitalWrite(keys_pin[i], LOW);
  }
  delay(1000);
}

void loop() {
  //если нет перегрева
  if (!hot_alarm) {
    if (!menu) {
      //делам прогон всех цифр во избежание "Отравление" катодов ламп раз в час
      if (millis() - last_time_scroll > scroll_int ) {
        scroll(500);
        if (debug) Serial.println("Scroll");
        last_time_scroll = millis();
      } else if (last_time_scroll > millis()) {
        last_time_scroll = millis();
      }

      if (!digitalRead(but_s)) {
        if (!butLong) {
          butPressT = millis();
          butLong = true;
          if (debug)Serial.println("setup");
        } else if (millis() - butPressT > (menudelay * 1000)&butPressT != 0) {
          if (debug)Serial.println("long");
          menu_init();
        }
      } else butLong = false;

      //для отчета выводим дату и време и другие параметры
      if (millis() >= curTime + interval ) {
        for (int i = 0; i < 4; i++) {
          if (debug)Serial.print(time_date[i]);
        }
        if (debug) Serial.println();
        if (debug) digitalClockDisplay();

        led_ctrl();
        temp_ctrl();
        curTime = millis();
      } else if (curTime > millis()) {
        curTime = millis();
      }

      set_time();//смотрим нет ли в порте нового времени
      check_time_date();//запихиваем новое время в массив
      show(time_date);//отображаем на лампах
      dot_chek();//зажигаем или тушим точку

    } else {
      //
      //if (debug) digitalClockDisplay();
      check_time_date();
      show(time_date);//отображаем на лампах
      buton_handler();
    }
  } else {
    //перегрева, вафлим отображение
    do_idle();
    temp_ctrl();
  }
}

//==============================================================================

int par_chek(bool inc, bool hours) {
  if (hours) {
    if (inc) {
      if (h >= 23) h = 0; else h++;
      return h;
    } else {
      if (h == 0) h = 23; else h--;
      return h;
    }
  } else {
    if (inc) {
      if (m >= 59) m = 0; else m++;
      return m;
    } else {
      if (m == 0) m = 59; else m--;
      return m;
    }
  }
}

void change_time(bool left) {
  if (min_select || hour_select) {
    //значит сейчас декрементим выбраное значение
    if (left)
      if (debug)Serial.println("dec");
      else if (debug)Serial.println("inc");
    if (left) {
      //декремент
      if (min_select) m = par_chek(false, false);
      if (hour_select) h = par_chek(false, true);
    } else {
      //инкремент
      if (min_select) m = par_chek(true, false);
      if (hour_select) h = par_chek(true, true);
    }
  } else {
    h_m_swap = !h_m_swap;
  }
  if (left) {
    if (debug)Serial.println("Press left");
  } else {
    if (debug)Serial.println("Press right");
  }
  delay(200);
}

void par_accept() {
  min_select = false;
  hour_select = false;

  if (debug)Serial.print("h= ");
  if (debug)Serial.print(h);
  if (debug)Serial.print("m= ");
  if (debug)Serial.println(m);

  tmElements_t tm;
  RTC.read(tm);
  tm.Hour = h;
  tm.Minute = m;
  RTC.write(tm);
  setSyncProvider(RTC.get);
}

void buton_handler() {
  bool s = !digitalRead(but_s);
  bool r = !digitalRead(but_r);
  bool l = !digitalRead(but_l);
  if (l && r) {
    menu = false;//выход из меню
    min_select = false;
    hour_select = false;
    if (debug)Serial.println("Press exit");
    delay(200);
  } else {
    if (l) {
      change_time(true);
    }
    if (r) {
      change_time(false);
    }
    if (s) {
      if (min_select || hour_select) {
        //значит нужное значение установлено и нужно его применить
        par_accept();
        if (debug)Serial.println("accept");
      } else if (h_m_swap) {
        min_select = true;
        hour_select = false;
      } else {
        hour_select = true;
        min_select = false;
      }
      if (debug)Serial.println("Press select");
      delay(200);
    }
  }
}
